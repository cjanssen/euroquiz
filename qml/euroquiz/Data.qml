import QtQuick 2.0

ListModel {
    id: quiz

    ListElement {
        question:"Which EU member state has the highest rate of lactose intolerance? (2009)"
        answers:
            [ListElement {answer:"Italy"}, ListElement {answer:"Sweden"}, ListElement {answer:"Greece"}, ListElement {answer:"France"}]
        correct:1
        additionalInfo:"56% of all Italians suffer from lactose intolerance (Source: EFSA)"
    }

    ListElement {
        question:"What is the most important topic for Germans concerning their country? (2013)"
        answers:
            [ListElement {answer:"Immigration"}, ListElement {answer:"Inflation"}, ListElement {answer:"Taxes"}, ListElement {answer:"Unemployment"}]
        correct:2
        additionalInfo:
            "Inflation is the most important topic for Germans (25%) (Source: Eurobarometer)"
    }

    ListElement {
        question: "What was the primary source of energy production in 2012 in the EU member states?"
        answers: [ListElement {answer:"Gas"},
            ListElement {answer:"Solid fuels"},
            ListElement {answer:"Renewable energies"},
            ListElement {answer:"Nuclear heat"}]
        correct: 4
        additionalInfo: "The primary energy production in the EU28 was nuclear heat (Source: Eurostat)"
    }



//    ListElement {
//        question:
//            "In the age group of people aged 25-34 in the EU, which country has the highest gender pay gap? (2012)"
//        answers:
//            [ListElement {answer:"Slovakia"}, ListElement {answer:"Czech Republic"}, ListElement {answer:"United Kingdom"}, ListElement {answer:"Cyprus"}]
//        correct:1
//        additionalInfo:
//            "Slovakia has the highest gender pay gap (12,4%) (Source: Eurostat)"
//    }

    ListElement {
        question: "Which European cow breed is at risk of extinction?"
        answers: [
            ListElement {answer:"Norwegian Red cow"},
            ListElement {answer:"Charolais cow"},
            ListElement {answer:"Arouquesa cow"},
            ListElement {answer:"Lativan Blue cow"}
        ]
        correct: 4
        additionalInfo: "Latvian Blue cows graze in the littoral meadows in Mērsrags"
    }

    ListElement {
        question: "Which EU member state has a voting age of 16?"
        answers: [
            ListElement {answer:"Sweden"},
            ListElement {answer:"Austria"},
            ListElement {answer:"Denmark"},
            ListElement {answer:"Spain"}]
        correct: 2
        additionalInfo: "Austria is the only state with a voting age of 16"
    }

    ListElement {
        question:
            "How many deaths in the EU are caused by smoking each year? (2013)"
        answers:
            [ListElement {answer:"approx. 200.000"}, ListElement {answer:"approx. 300.000"}, ListElement {answer:"approx. 450.000"}, ListElement {answer:"approx. 700.000"}]
        correct:4
        additionalInfo:
            "Approximately 700.000 Europeans die each year due to smoking (Source: ENSP)"
    }
   // +++ 26.2. In the future 2/3 of the cigarette packs will be marked with health warnings +++ (Agree / Disagree / Don’t Care)
//+++ An EU-wide binding target for renewable energy of at least 27% is an important goal of the new EU framework on climate and energy for 2030 +++
}
