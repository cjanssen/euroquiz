import QtQuick 2.0

Rectangle {
    id:login
    anchors.fill:parent
    color: "#3c3c3c"

    Image {
        anchors.top: parent.top
        anchors.fill: parent
        source: "qrc:////img/euflag.png"
    }

    Image {
        source: "qrc:////img/logo.png"
        width: 1200
        fillMode: Image.PreserveAspectFit
    }

    Column {
        spacing: 30
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 600

        Image {
            source: "qrc:////img/fblogin.png"
            width: 900
            height: 186
        }

        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            width: 186
            height: 186
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            color: "white"
            font.pointSize: 54
            font.bold: true
            text: "or"
        }


        InputButton {
            text: "Maria91"
            hasArrows: false
        }

        InputButton {
            text: "Poland"
            hasArrows: true
        }

        PushButton {
            text: "Play"
            onClicked: root.startGame()
        }

    }
}
