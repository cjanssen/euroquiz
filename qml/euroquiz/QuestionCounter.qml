import QtQuick 2.0

Rectangle {
    id: questionCounter
    width: 144
    height: 96
    radius: 12
    property int qid: 0
    property string text
    color: {
        if (qid > question.qNumber)
            return customDarkerGray;
        else if (qid == question.qNumber && question.active)
            return "white";
        else if (qStates[qid])
            return customGreen;
        else
            return customRed;
    }
    Text {
        anchors.centerIn: parent
        text: questionCounter.text
        font.bold: true
        font.pointSize: 30
    }
}
