import QtQuick 2.0

Rectangle {
    id:splash
    anchors.fill:parent
   // color: "#3c3c3c"

    function nextScreen() {
        splash.visible = false
        loginScreen.visible = true
    }

    Image {
        anchors.top: parent.top
        anchors.fill: parent
        source: "qrc:////img/euflag2.png"
    }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                nextScreen()
            }
        }
    }

