import QtQuick 2.0

Rectangle {
    id: inputButton

    width: 900
    height: 186
    color: "white"
    radius: 15


    property string text
    anchors.horizontalCenter: parent.horizontalCenter
    property bool hasArrows: false
//    property int buttonid
//    property bool correct: false


    Rectangle {
        color: "#e2e2e2"
        width: parent.width - 36
        height: parent.height - 36
        anchors.centerIn: parent
        radius: 12

        Text {
            id: inputText
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: 24
            color: "black"
            text: inputButton.text
            font.bold: true
            font.pointSize: 40
        }

        Image {
            source: "qrc:////img/arrows.png"
            visible: hasArrows
            anchors.right: parent.right
            anchors.rightMargin: 45
            anchors.verticalCenter: parent.verticalCenter
            height: parent.height - 45
            fillMode: Image.PreserveAspectFit

        }
    }

//    MouseArea {
//        anchors.fill: parent
//        onClicked: {
//            // nothing
//            question.evaluateButtons(buttonid);
//        }
//    }
}
