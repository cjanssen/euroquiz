import QtQuick 2.0

Rectangle {
    id: pushButton

    width: 900
    height: 186
    color: isSelected?customGreen:"white"
    radius: 15

    signal clicked

    property bool isSelected: false


    property string text
    anchors.horizontalCenter: parent.horizontalCenter

    Text {
        id: inputText
        anchors.centerIn: parent
        color: isSelected?"white":"black"
        text: pushButton.text
        font.bold: true
        font.pointSize: 40
    }


        MouseArea {
            anchors.fill: parent
            onClicked: {
                pushButton.clicked()
            }
        }
}
