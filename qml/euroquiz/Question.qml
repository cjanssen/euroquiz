import QtQuick 2.0

Rectangle {
    id:question
    anchors.fill:parent
    color: "#3c3c3c"

    property int qNumber: 0
    property bool wasCorrect: false
    property bool active: true

    signal evaluateButtons(int triggeredId)
    signal timeOver()
    signal reset()
    signal done()


    Connections {
        target:question
        onEvaluateButtons: done()
        onTimeOver: done()
        onDone: {
                    question.active = false
                    contineButton.visible = true
                    adinfo.visible = true
                }
    }

    Data {
        id: quiz
    }

    Image {
        anchors.top: parent.top
        anchors.fill: parent
        source: "qrc:////img/euflag.png"
    }

    Item {
        width: 900
        anchors.horizontalCenter: parent.horizontalCenter
        height: 186
        Text {
            color: "#c2c2c2"
            font.pointSize: 32
            anchors.horizontalCenter: parent.horizontalCenter
            y: 12
            font.bold: true
            text: "Playing against: AlbrechtDürer89"
        }
        Row {
            spacing: 180
            anchors.horizontalCenter: parent.horizontalCenter
            y: 102
            QuestionCounter {
                qid: 0+qBlock*3
                text: qLabels[qid]
            }
            QuestionCounter {
                qid: 1+qBlock*3
                text: qLabels[qid]
            }
            QuestionCounter {
                qid: 2+qBlock*3
                text: qLabels[qid]
            }
        }
    }

    Column {
        spacing: 30
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 300

//        Text {
//          text: "score 10"
//        }

        QuestionShow {
            text: quiz.get(question.qNumber).question
        }

        Repeater {
            model: 4

            AnswerButton {
                buttonid: index
                correct: quiz.get(question.qNumber).correct == index+1
                text: quiz.get(question.qNumber).answers.get(index).answer
            }

        }

        Item {
            id: invisibleBtn
            visible: false
            width: 900
            height: 186
        }

        QuestionShow {
            id:adinfo
            fontSize: 36
            text: quiz.get(question.qNumber).additionalInfo
            visible: false
        }

        PushButton {
            id: contineButton
            text: "Continue"
            visible: false
            onClicked: {
                question.qNumber++;
                qBlock = Math.floor(question.qNumber/3)
                adinfo.visible = false
                contineButton.visible = false
                invisibleBtn.visible = false
                question.active = true

                if (question.qNumber==3) {
                    question.visible = false
                    waitScreen.visible = true

                } else {
                    question.reset();
                }

                if (question.qNumber>=6) {
                    question.visible = false
                    opinion.visible = true
                }
            }
        }


        TimerDisplay {

        }
    }

    // reset test
//    MouseArea {
//        id: resetMA
//        anchors.fill: parent
//        enabled: false
//        Connections {
//               target: question
//               onDone: {
//                   root.qStates[question.qNumber] = wasCorrect
//                   contineButton.visible = true
//                   adinfo.visible = true
//               }
//        }
//        onClicked: {

//        }
//    }

}
