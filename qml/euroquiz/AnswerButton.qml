import QtQuick 2.0

Rectangle {
    id: answerButton

    width: 900
    height: 186
    color: "white"
    radius: 15


    property string text
    anchors.horizontalCenter: parent.horizontalCenter
    property int buttonid
    property bool correct: false

    Connections {
        target: question
        onEvaluateButtons: {
            if (correct) {
                if (triggeredId == buttonid) {
                    answerButton.color = customGreen;
                    answerText.color = "white"
                    invisibleBtn.visible = true
                    root.qStates[question.qNumber] = true
                    score++
                } else {
//                    answerButton.color = customRed;
                    flashTimer.restart();
                }

            } else {
                if (triggeredId == buttonid) {

                    answerButton.color = customRed;
                    answerText.color = "white"

                } else {
                    answerButton.color = question.color
                    answerText.color = question.color
                    visible = false
                }
            }
            enabled = false
        }
        onTimeOver: {
            enabled = false
            if (correct) {
//                answerButton.color = customRed;
//                answerText.color = "white"
                flashTimer.restart();
            } else {
                answerButton.color = question.color
                answerText.color = question.color
                invisibleBtn.visible = true
                visible = false
            }
            enabled = false
        }
        onReset: {
            visible = true
            enabled = true
            answerButton.color = "white"
            answerText.color = "black"
            flashTimer.stop()
            flashTimer.itercount = flashTimer.intialIterCount
        }
    }

    Timer {
        id: flashTimer
        interval: 300
        running: false
        repeat: true
        property int intialIterCount: 7
        property int itercount: 7
        triggeredOnStart: true
        onTriggered: {
            if (itercount-- % 2) {
                answerButton.color = customGreen;
                answerText.color = "white"
            } else {
                answerButton.color = customGray;
                answerText.color = customDarkerGray;
            }
            if (!itercount) {
                stop();
                itercount = intialIterCount;
            }
        }
    }


    Text {
        id: answerText
        anchors.centerIn: parent
        color: "black"
        text: parent.text
        font.bold: true
        font.pointSize: 34
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            // nothing
            question.evaluateButtons(buttonid);
            question.wasCorrect = correct;
        }
    }
}
