import QtQuick 2.0

Rectangle {
    id: winscr
    anchors.fill:parent
    color: "#3c3c3c"

    Image {
        anchors.top: parent.top
        anchors.fill: parent
        source: "qrc:////img/euflag.png"
    }

    Text {
        id:winText
        y: 700
        anchors.horizontalCenter: parent.horizontalCenter
        color: "white"
        font.pointSize: 28
        horizontalAlignment: Text.AlignHCenter
        text: "Thank you for playing EuroQuiz.\nYou scored "+score+" out of 6."
        font.bold: true
    }

    PushButton {
        anchors.top: winText.bottom
        anchors.topMargin: 60
        text: "New Game"
        onClicked: {
            loginScreen.visible = true
            questionScreen.visible = false
            opinion.visible = false
            win.visible = false
            waitScreen = false
            root.qStates = [false,false,false,false,false]
            root.qBlock = 0
            questionScreen.qNumber = 0
            questionScreen.active = true
            questionScreen.wasCorrect = false
//            startGame()
            opinion.selectedId = -1
            opinion.opinionText = ""
            score = 0

        }

    }


}
