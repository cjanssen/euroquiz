import QtQuick 2.0

Item {
    id: timerDisplay
    width: 900
    height: 186

    property real totalTime: 20
    property real currentTime: totalTime
    property int displayTime: currentTime

    function start() {
        visible = true
        currentTime = totalTime
        timerControl.restart()
    }

    Connections {
        target: question
        onEvaluateButtons: timerControl.stop()
        onReset: start()
        onDone: visible = false
    }

    Timer {
        id: timerControl
        running: false
        interval: 100
        repeat: true
        onTriggered: {
            if (currentTime > 0) {
                currentTime = currentTime - 0.1;
            } else {
                timerControl.stop();
                question.timeOver();
            }
        }
    }

    Item {
        width: parent.width
        height: 96
        anchors.centerIn: parent
        Rectangle {
            property int minwidth : timerbox.width * 0.9
            width: (parent.width-minwidth) * currentTime /totalTime + minwidth
            color: {
                if (currentTime > totalTime*3/4)
                    return Qt.rgba(21/256,255/256,0,1);
                else if (currentTime > totalTime*1/2)
                    return Qt.rgba(209/256, 231/256, 0,1);
                else if (currentTime > totalTime*1/4)
                    return Qt.rgba(255/256,139/256,9/256,1);
                else
                    return Qt.rgba(255/256,9/256,9/256,1);
            }
            radius: 15
            height: parent.height /2
            anchors.centerIn: parent
        }


        Rectangle {
            id: timerbox
            anchors.horizontalCenter: parent.horizontalCenter
            color: "#575757"
            height: parent.height
            width: height
            radius: 15
            Text {
                color: "white"
                anchors.centerIn: parent
                font.pointSize: 36
                font.bold: true
                text: Math.ceil(currentTime)
            }

        }
    }

}
