import QtQuick 2.0
import QtQuick.Window 2.0

Rectangle {
    id: root
    width: 400
    height: 640
//    width: 1200
//    height: 1920



    scale: width/1200

    property color customGreen : Qt.rgba(21/256,255/256,0,1)
    property color customRed: Qt.rgba(255/256,9/256,9/256,1)
    property color customGray: "gray"
    property color customDarkerGray: "darkgray"


    function startGame() {
        questionScreen.visible = true
        loginScreen.visible = false
        questionScreen.reset()
    }

    property var qStates : [false,false,false,false,false]
    property var qLabels: ["I","II","III","IV","V","VI"]
    property int qBlock: 0
    property int score : 0

    Rectangle {
        width: 1200
        height: 1920
        scale: 1/root.scale
        color:"black"
    }


    Item {
        width:1200
        height:1920
        x:root.scale<1? -width*(1-root.scale)/2 : 0
        y:root.scale<1? -height*(1-root.scale)/2 : 0

        Splash {
            id: splashScreen
        }

        Login {
            id: loginScreen
            visible: false
        }



        Question {
            id: questionScreen
            visible: false
        }


        Wait {
            id: waitScreen
            visible: false
        }

        Opinion {
            id: opinion
            visible: false
        }

        WinScreen {
            id: win
            visible: false
        }



    }



//    MouseArea {
//        anchors.fill: parent
//        onClicked: {
//            Qt.quit();
//        }
//    }
}
