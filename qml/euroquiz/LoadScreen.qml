import QtQuick 2.0

Rectangle {
    id:login
    anchors.fill:parent
    color: "#3c3c3c"

    Image {
        anchors.top: parent.top
        anchors.fill: parent
        source: "qrc:////img/euflag.png"
    }

    Column {
        spacing: 30
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 600


        Text {
            text: "Searching for player..."
            color: "white"
            font.bold: true
            font.size: 36
        }

    }
}
