import QtQuick 2.0

Rectangle {
    id:opinion
    anchors.fill:parent
    color: "#000129"

    function nextScreen() {
        opinion.visible = false
        win.visible = true
    }

    property int selectedId:-1
    property string opinionText: ""



    Column {
        spacing: 30
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.topMargin: 100

//        Text {
//          text: "score 10"
//        }

        QuestionShow {
            fontSize: 32
            text: "Health warnings will cover two thirds of cigarette packs following final Parliamentary approval of new tobacco products legislation on 26 February 2014."
        }

//        Item {
//            id: invisibleBtn
////            visible: false
//            width: 900
//            height: 186
//        }

        PushButton {
            id: agButton
            text: "Agree"
            isSelected: selectedId == 0
            onClicked: {
                selectedId = 0
            }
        }

        PushButton {
            id: disagButton
            text: "Disagree"

            isSelected: selectedId == 1
            onClicked: {
                selectedId = 1
            }
        }

        PushButton {
            id: dontcButton
            text: "Don't care"
            isSelected: selectedId == 2
            onClicked: {
                selectedId = 2
            }
        }

        Text {
            color: "white"
            font.pixelSize: 42
            text: "Tell us why"
            font.bold: true
            anchors.horizontalCenter: parent.horizontalCenter
        }

        Rectangle {
            width: 900
            height: 272
            radius: 15
            color:"gray"
            anchors.horizontalCenter: parent.horizontalCenter

            TextInput {
                id: opinionInput
                width: parent.width - 30
                height: parent.height - 30
                anchors.centerIn: parent
                wrapMode: Text.WrapAnywhere
                font.bold: true
                font.pixelSize: 32
                color: "white"
                text: opinionText
                onAccepted: Qt.inputMethod.hide()
            }
        }

        Item {
            width: 900
            height: 62
            Image {
                source:"qrc:///img/shareon.png"
                height: 62*2
                width: 570
                fillMode: Image.PreserveAspectFit
            }

            Rectangle {
                width: 300
                height: 62*2
                x: parent.width - width
                color: "white"
                radius: 15

                Text {
                    id: inputText
                    anchors.centerIn: parent
                    color: "black"
                    text: "Continue"
                    font.bold: true
                    font.pointSize: 28
                }


                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            nextScreen()
                        }
                    }
            }

        }


    }


}
